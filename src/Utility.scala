import java.io.File

import scala.io.Source
import scala.swing.FileChooser

/**
  * Created by keith on 30/10/2015.
  */
object Utility extends App {
  /** Asks the user to choose a plain file.
    *
    * @param title What to put in the file chooser dialog title bar.
    * @return The name of the chosen file.
    */
  def choosePlainFile(title: String = ""): Option[File] = {
    val chooser = new FileChooser(new File("."))
    chooser.title = title
    val result = chooser.showOpenDialog(null)
    if (result == FileChooser.Result.Approve) {
      println("Approve -- " + chooser.selectedFile)
      Some(chooser.selectedFile)
    } else None
  }

  /** Asks the user to choose a directory, then returns (as an option)
    * an array of the files in that directory.
    *
    * @param title What to put in the file chooser dialog title bar.
    * @return The files in the chosen directory.
    */
  def getDirectoryListing(title: String = ""): Option[Array[File]] = {
    val chooser = new FileChooser(null)
    chooser.fileSelectionMode = FileChooser.SelectionMode.DirectoriesOnly
    chooser.title = title
    val result = chooser.showOpenDialog(null)
    if (result == FileChooser.Result.Approve) {
      Some(chooser.selectedFile.listFiles())
    } else None
  }

  /** Returns the words from a given text, where a word is defined
    * as a nonempty sequence of letters only.
    *
    * @param text The input text.
    * @return A list of words from the text.
    */
  def getWordsFromText(text: String): List[String] = {
    ("""[a-zA-Z]+""".r findAllIn text) toList
  }

  // Example usage
/*
  val s = Utility.choosePlainFile("input")

  if (s.isDefined) {

    for {
      line <- Source fromFile s.get getLines
    } {
      print(getWordsFromText(line))
    }
  }*/

  //My example usage of other method
  val s2 = Utility.getDirectoryListing("input")

  if (s2.isDefined){
   for {file <- s2.get} println(file)
  }
}
